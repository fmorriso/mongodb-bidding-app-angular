# Bidding Web Application

This is a web application demonstrating real-time event delivery with [MongoDB Atlas App Services](https://mongodb.com/docs/atlas/app-services/?utm_campaign=devrel&utm_source=cross-post&utm_medium=cta&utm_content=github-bidding-app&utm_term=stanimira.vlaeva) and [Realm Web SDK](https://mongodb.com/docs/realm/web/?utm_campaign=devrel&utm_source=cross-post&utm_medium=cta&utm_content=github-bidding-app&utm_term=stanimira.vlaeva). The app is built with Angular.

## History
* 2024-05-09 Started from baseline clone
## References
[Building a Real-Time Web App with MongoDB Atlas App Services](https://www.youtube.com/watch?v=YixtWTGUyFk)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Disclaimer

Use at your own risk; not a supported MongoDB product
